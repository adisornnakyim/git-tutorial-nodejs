const express = require('express');
const app = express();
/* เมื่อ Browser (req) เรียกมาที่ past /hello แล้วต้องการส่ง messge:'test' กลับไปที่ Browser  localhost:8080' */
app.get('/hello', (req, res) => {
  res.json({
    messge: 'test'
  });
});

app.get('/pleum', (req, res) => {
  res.json({
    name: 'Chacree'
  });
});

app.listen(8080);
